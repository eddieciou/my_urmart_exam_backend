from django.http import HttpResponse, HttpResponseNotFound

from .models import Report
from shop.models import Order


# Create your views here.
def report(request):
    if request.method != "GET":
        return HttpResponseNotFound('<h1>Not found</h1>')

    report_query_set = Order.sale_report()
    for report_dict in report_query_set:
        report_model = Report.get_by_shop_id(report_dict['shop_id'])
        if report_model:
            report_model.total_quantity = report_dict['total_quantity']
            report_model.total_amount = report_dict['total_amount']
            report_model.order_count = report_dict['order_count']
        else:
            report_model = Report(
                shop_id=report_dict['shop_id'],
                total_quantity=report_dict['total_quantity'],
                total_amount=report_dict['total_amount'],
                order_count=report_dict['order_count'],
            )

        report_model.save()

    return HttpResponse("ok")
