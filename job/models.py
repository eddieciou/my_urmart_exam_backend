from datetime import timedelta
from django.db import models


# Create your models here.
class Report(models.Model):
    shop_id = models.CharField(max_length=2, help_text='館別')
    total_quantity = models.IntegerField(help_text='總銷售數量')
    total_amount = models.IntegerField(help_text='總銷售總金額')
    order_count = models.IntegerField(help_text='總訂單數量')
    update_datetime = models.DateTimeField(auto_now=True)

    def get_json(self):
        return {
            'shop_id': self.shop_id,
            'total_quantity': self.total_quantity,
            'total_amount': self.total_amount,
            'order_count': self.order_count,
            'update_datetime': str(self.update_datetime + timedelta(hours=8))[0:19],
        }

    @staticmethod
    def get_by_shop_id(shop_id):
        query_set = Report.objects.filter(shop_id=shop_id)
        if query_set:
            return list(query_set)[0]
