from django.contrib import admin

from .models import Report
# Register your models here.


@admin.register(Report)
class ReportAdmin(admin.ModelAdmin):
    list_display = ('shop_id', 'total_quantity', 'total_amount', 'order_count', 'update_datetime')
