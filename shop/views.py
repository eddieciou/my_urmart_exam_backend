from django.http import HttpResponseNotFound
from django.shortcuts import render, redirect

from .decorators import order_check
from .models import Product, Order
from job.models import Report


# Create your views here.
@order_check
def index(request, form):
    message = request.session.get('message')
    request.session.clear()

    products = Product.objects.all()
    orders = Order.objects.all()
    reports = Report.objects.all()

    if request.method == 'POST':
        product = form.cleaned_data['product']
        order = Order(
            product=product,
            quantity=form.cleaned_data['quantity'],
            price=form.cleaned_data['quantity'] * product.price,
            shop_id=product.shop_id,
            customer_id=form.cleaned_data['customer_id'],
        )
        order.save()

        request.session['message'] = '訂單建立成功'
        return redirect('/shop')

    context = {
        'products': [product.get_json for product in products],
        'orders': [order.get_json for order in orders],
        'top_3': Order.top_3(),
        'reports': [report.get_json for report in reports],
        'form': form,
        'message': message
    }
    return render(request, 'index.html', context=context)


def delete(request, order_number):
    if request.method != "GET":
        return HttpResponseNotFound('<h1>Not found</h1>')

    order_model = Order.objects.get(pk=order_number)
    product_model = order_model.product
    origin_stock = product_model.stock_pcs
    product_model.stock_pcs = origin_stock + 1
    order_model.delete()
    product_model.save()

    if origin_stock == 0:
        request.session['message'] = f'商品到貨{str(product_model)}'

    return redirect('index')
