from django.shortcuts import render

from .forms import OrderForm
from .models import Product, Order
from job.models import Report


def order_check(func):
    def wrapper(request):
        form = OrderForm()
        if request.method == 'POST':
            form = OrderForm(request.POST)
            if form.is_valid():
                args = form.cleaned_data
                product = args['product']

                error_message = ''
                if product.stock_pcs < args['quantity']:
                    error_message = f'貨源不足，剩餘庫存：{product.stock_pcs}'
                if product.vip and not args['is_vip']:
                    error_message = '此產品需為VIP身份'
                if error_message:
                    return render(request, 'index.html', context={
                        'products': [product.get_json for product in Product.objects.all()],
                        'orders': [order.get_json for order in Order.objects.all()],
                        'top_3': Order.top_3(),
                        'reports': [report.get_json for report in Report.objects.all()],
                        'form': form,
                        'error_message': error_message
                    })

                # 驗證完後先扣掉庫存
                product.stock_pcs = product.stock_pcs - args['quantity']
                product.save()
            else:
                return render(request, 'index.html', context={
                    'products': [product.get_json for product in Product.objects.all()],
                    'orders': [order.get_json for order in Order.objects.all()],
                    'top_3': Order.top_3(),
                    'reports': [report.get_json for report in Report.objects.all()],
                    'form': form
                })

        return func(request, form=form)
    return wrapper
