from django.db import models
from django.db.models import Count, Sum

# Create your models here.


class Product(models.Model):
    stock_pcs = models.IntegerField(help_text='剩餘庫存數量')
    price = models.IntegerField(help_text='價格')
    shop_id = models.CharField(max_length=2, help_text='館別')
    vip = models.BooleanField(default=False, help_text='VIP限定')

    def __str__(self):
        """String for representing the Model object."""
        return f' Product ID:{self.id} 館別:{self.shop_id}'

    class Meta:
        ordering = ['id']

    def get_json(self):
        return {
            'id': self.id,
            'stock_pcs': self.stock_pcs,
            'price': self.price,
            'shop_id': self.shop_id,
            'vip': self.vip
        }


class Order(models.Model):
    product = models.ForeignKey('Product', on_delete=models.DO_NOTHING)
    quantity = models.IntegerField(help_text='數量')
    price = models.IntegerField(help_text='總金額')
    shop_id = models.CharField(max_length=2, help_text='館別')
    customer_id = models.IntegerField(help_text='客戶ID')

    class Meta:
        ordering = ['-id']

    def __str__(self):
        """String for representing the Model object."""
        return f'訂單編號:{self.id}'

    def get_json(self):
        return {
            'id': self.id,
            'product_id': self.product.id,
            'quantity': self.quantity,
            'price': self.price,
            'shop_id': self.shop_id,
            'customer_id': self.customer_id
        }

    @staticmethod
    def sale_report():
        return Order.objects.values('shop_id').distinct().order_by('shop_id').annotate(
            total_quantity=Sum('quantity'), total_amount=Sum('price'), order_count=Count('shop_id')
        )

    @staticmethod
    def top_3():
        query_set = Product.objects.annotate(sale_count=Sum('order__quantity')).order_by('-sale_count'). \
                        filter(sale_count__isnull=False)[:3]

        return [str(product) for product in query_set]
