from django import forms
from .models import Product


class OrderForm(forms.Form):
    my_default_errors = {
        'required': 'This field is required',
        'invalid': 'Enter a valid value'
    }
    product = forms.ModelChoiceField(queryset=Product.objects.all(), empty_label='Select Product')
    quantity = forms.IntegerField(widget=forms.TextInput(attrs={'placeholder': '數量', 'style': 'margin-left:20px'}))
    customer_id = forms.IntegerField(widget=forms.TextInput(attrs={'placeholder': 'Customer ID'}))
    is_vip = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={'style': 'margin-right:10px'}),
        label='是否為VIP身份',
        required=False
    )
